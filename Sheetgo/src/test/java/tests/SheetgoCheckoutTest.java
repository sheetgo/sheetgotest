package tests;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class SheetgoCheckoutTest {

    private WebDriver navegador;

    @Before
    public void setUp(){
        //Abrindo o navegador
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Drivers\\chromedriver.exe");
        navegador = new ChromeDriver();
        navegador.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

        //Navegando para a pagina do taskit
        navegador.get("http://dev.sheetgo.com");

        //Clicar no botao para logar com o google com o xpath "//*[@id="google-login"]/span/div[2]"
        navegador.findElement(By.xpath("//*[@id=\"google-login\"]/span/div[2]")).click();

        //Abrir popup para login
        System.out.println(navegador.getWindowHandle());
        System.out.println(navegador.getWindowHandles());
        navegador.switchTo().window((String) navegador.getWindowHandles().toArray()[1]);

        //Verificar se abriu o oauth2 com o titulo Fazer login
        WebElement mensagemPop = navegador.findElement(By.xpath("//*[@id=\"headingText\"]/span"));
        String mensagem = mensagemPop.getText();
        assertEquals("Fazer login", mensagem);

        //Escrever no input com xpath "//*[@id="identifierId"]" sheetgotest@gmail.com
        navegador.findElement(By.xpath("//*[@id=\"identifierId\"]")).sendKeys("user-sheetgo-130@gedu-demo-sheetgo.com");

        //Clicar no button com xpath "//*[@id="identifierNext"]/div/button"
        navegador.findElement(By.xpath("//*[@id=\"identifierNext\"]/div/button")).click();

        //Escrever no input com name "password" Sheetgo2020
        navegador.findElement(By.name("password")).sendKeys("toolsmoon");

        //Clicar no button com xpath "//*[@id="passwordNext"]/div/button"
        navegador.findElement(By.xpath("//*[@id=\"passwordNext\"]/div/button")).click();

        navegador.switchTo().window((String) navegador.getWindowHandles().toArray()[0]);

        {
            WebDriverWait wait = new WebDriverWait(navegador, 15);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("account-menu-button")));
        }

        //Clicar no avatar
        navegador.findElement(By.id("account-menu-button")).click();

        //Clicar em Manage account
        navegador.findElement(By.cssSelector(".MuiMenuItem-root:nth-child(1) .MuiTypography-root")).click();

        // ir ate checkout
        navegador.get("https://dev.sheetgo.com/account/checkout");

    }


    // @Test
    public void changePlans(){
        // PARA INICIAR ESSE TESTE CERTIFICAR-SE QUE O PLANO INICIAL E O EXPERT //

        //Mudar do plano Expert para o plano Profissional
        navegador.findElement(By.xpath("//*[@id=\"app\"]/div[1]/div[4]/div[2]/div/div/div[2]/div[5]/div/div/div[1]/div[4]/button/span")).click();
        navegador.findElement(By.xpath("/html/body/div[5]/div[3]/div/div[2]/div[2]/div[3]/button[2]/span")).click();
        navegador.findElement(By.xpath("/html/body/div[5]/div[3]/div/div/div/div[3]/button")).click();

        //Mudar do plano Profissional para o plano Advanced
        //navegador.findElement(By.xpath("///*[@id=\"app\"]/div[1]/div[3]/div[2]/div/div/div[2]/div[4]/div/div/div[1]/div[4]/button/span")).click();
        //navegador.findElement(By.xpath("/html/body/div[5]/div[3]/div/div[2]/div[2]/div[3]/button[2]/span")).click();
        //navegador.findElement(By.xpath("/html/body/div[5]/div[3]/div/div/div/div[3]/button")).click();

        //Mudar do plano Advanced para o plano Starter
        //navegador.findElement(By.xpath("//*[@id=\"app\"]/div[1]/div[3]/div[2]/div/div/div[2]/div[3]/div/div/div[1]/div[4]/button/span/span")).click();
        //navegador.findElement(By.xpath("/html/body/div[5]/div[3]/div/div[2]/div[2]/div[3]/button[2]/span")).click();
        //navegador.findElement(By.xpath("/html/body/div[5]/div[3]/div/div/div/div[3]/button")).click();

    }

    //@Test
    public void cancelPlan(){
        // ir para subscription
        navegador.get("https://dev.sheetgo.com/account/subscription");
        // Clicar no botao
        navegador.findElement(By.xpath("//*[@id=\"app\"]/div[1]/div[4]/div[2]/div/div[1]/div/div[2]/button")).click();
        //Escolher opcao de cancelamento
        assertThat(navegador.findElement(By.cssSelector("#material-dialog-title > .MuiTypography-root")).getText(), is("Subscription cancellation"));
        {
            WebElement element = navegador.findElement(By.cssSelector(".MuiSelect-root"));
            Actions builder = new Actions(navegador);
            builder.moveToElement(element).clickAndHold().perform();
        }
        {
            WebElement element = navegador.findElement(By.cssSelector(".MuiMenuItem-root:nth-child(1)"));
            Actions builder = new Actions(navegador);
            builder.moveToElement(element).release().perform();
        }
        navegador.findElement(By.xpath("//*[@id=\"menu-\"]/div[3]/ul/li[2]")).click();
        navegador.findElement(By.xpath("//*[@id=\"message\"]")).sendKeys("n/a");
        navegador.findElement(By.cssSelector(".MuiButton-textPrimary:nth-child(1) > .MuiButton-label")).click();


    }


    //@Test
    public void addCartDefault(){
        navegador.get("https://dev.sheetgo.com/account/subscription");
        navegador.findElement(By.xpath("//*[@id=\"app\"]/div[1]/div[4]/div[2]/div/div[2]/div[2]/button/span/span")).click();
        navegador.findElement(By.name("name")).click();
        navegador.findElement(By.name("name")).sendKeys("Teste");
        navegador.findElement(By.cssSelector(".StripeElement")).click();
        navegador.switchTo().frame(5);
        navegador.findElement(By.name("cardnumber")).sendKeys("4242 4242 4242 4242");
        navegador.findElement(By.name("exp-date")).sendKeys("10 / 27");
        navegador.findElement(By.name("cvc")).sendKeys("444");
        navegador.findElement(By.name("postal")).sendKeys("09855");
        //navegador.findElement(By.xpath("/html/body/div[6]/div[3]/div/div[3]/div/div/button/span")).click();
    }

    //@Test
    public void addCart3D(){

        navegador.get("https://dev.sheetgo.com/account/subscription");
        navegador.findElement(By.xpath("//*[@id=\"app\"]/div[1]/div[4]/div[2]/div/div[2]/div[2]/button/span/span")).click();
        navegador.findElement(By.name("name")).click();
        navegador.findElement(By.name("name")).sendKeys("Teste");
        navegador.findElement(By.cssSelector(".StripeElement")).click();
        navegador.switchTo().frame(5);
        navegador.findElement(By.name("cardnumber")).sendKeys("4000 0000 0000 3220");
        navegador.findElement(By.name("exp-date")).sendKeys("10 / 27");
        navegador.findElement(By.name("cvc")).sendKeys("444");
        navegador.findElement(By.name("postal")).sendKeys("09855");
       // navegador.findElement(By.xpath("xpath=//div[3]/div/div[3]/div/div/button/span")).click();


    }

    //@Test
    public void addCart3DFalha(){
        navegador.get("https://dev.sheetgo.com/account/subscription");
        navegador.findElement(By.xpath("//*[@id=\"app\"]/div[1]/div[4]/div[2]/div/div[2]/div[2]/button/span/span")).click();
        navegador.findElement(By.name("name")).click();
        navegador.findElement(By.name("name")).sendKeys("Teste");
        navegador.findElement(By.cssSelector(".StripeElement")).click();
        navegador.switchTo().frame(5);
        navegador.findElement(By.name("cardnumber")).sendKeys("4000000000003063");
        navegador.findElement(By.name("exp-date")).sendKeys("10 / 27");
        navegador.findElement(By.name("cvc")).sendKeys("444");
        navegador.findElement(By.name("postal")).sendKeys("09855");
       // navegador.findElement(By.cssSelector(".MuiButton-textPrimary:nth-child(1) > .MuiButton-label")).click();
    }

    @Test
    public void invoice(){
        //Ver comprovantes de pagamento
        navegador.get("https://dev.sheetgo.com/account/invoices");

    }

    //@After
    public void tearDown(){
        //Fechar o navegador
        //close fecha uma e o quit fecha todas
        navegador.quit();
    }
}
