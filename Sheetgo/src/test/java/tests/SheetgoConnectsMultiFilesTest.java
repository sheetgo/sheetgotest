package tests;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class SheetgoConnectsMultiFilesTest {
    private WebDriver navegador;

    @Before
    public void setUp(){

        ///
        //QUANDO EU FIZER LOGIN COM GOOGLE DEVE APARECER A TELA HOME
        ////

        //Abrindo o navegador
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Drivers\\chromedriver.exe");
        navegador = new ChromeDriver();
        navegador.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        //Navegando para a pagina do taskit
        navegador.get("http://dev.sheetgo.com");

        //Clicar no botao para logar com o google com o xpath "//*[@id="google-login"]/span/div[2]"
        navegador.findElement(By.xpath("//*[@id=\"google-login\"]/span/div[2]")).click();

        //Abrir popup para login
        System.out.println(navegador.getWindowHandle());
        System.out.println(navegador.getWindowHandles());
        navegador.switchTo().window((String) navegador.getWindowHandles().toArray()[1]);

        //Verificar se abriu o oauth2 com o titulo Fazer login
        WebElement mensagemPop = navegador.findElement(By.xpath("//*[@id=\"headingText\"]/span"));
        String mensagem = mensagemPop.getText();
        assertEquals("Fazer login", mensagem);

        //Escrever no input com xpath "//*[@id="identifierId"]" sheetgotest@gmail.com
        navegador.findElement(By.xpath("//*[@id=\"identifierId\"]")).sendKeys("sheetgotest@gmail.com");

        //Clicar no button com xpath "//*[@id="identifierNext"]/div/button"
        navegador.findElement(By.xpath("//*[@id=\"identifierNext\"]/div/button")).click();

        //Escrever no input com name "password" Sheetgo2020
        navegador.findElement(By.name("password")).sendKeys("Sheetgo2020");

        //Clicar no button com xpath "//*[@id="passwordNext"]/div/button"
        navegador.findElement(By.xpath("//*[@id=\"passwordNext\"]/div/button")).click();

        //Retornar para a tela oficial
        navegador.switchTo().window((String) navegador.getWindowHandles().toArray()[0]);

        ////
        // QUANDO EU FAZER O IMPORT COM GOOGLE SHEETS DEVE ME APARECER 4 OPCOES DE SEND DATA
        ////

        // 3 | click | css=.MuiFab-label |
        navegador.findElement(By.cssSelector(".MuiFab-label")).click();
        // 4 | click | css=.MuiButton-contained > .MuiButton-label |
        navegador.findElement(By.cssSelector(".MuiButton-contained > .MuiButton-label")).click();
        // 5 | click | xpath=//*[@id="app"]/div/div[1]/div[4]/div[3]/div/div/div/div/div[2]/div/div/div[2]/button/span |
        navegador.findElement(By.xpath("//*[@id=\"app\"]/div/div[1]/div[4]/div[3]/div/div/div/div/div[2]/div/div/div[2]/button/span")).click();
        // 6 | click | xpath=//*[@id="connection-builder"]/div/div/div/div[2]/div/div/div/div/div/div[2]/div/div[2]/div/p |
        navegador.findElement(By.xpath("//*[@id=\"connection-builder\"]/div/div/div/div[2]/div/div/div/div/div/div[2]/div/div[2]/div/p")).click();
        // 7 | click | xpath=//*[@id="connection-builder"]/div/div/div[1]/div[2]/div/div/div/div[1]/div/div/div/div/div/ul/li[2]/div[2]/div/div/button/span |
        navegador.findElement(By.xpath("//*[@id=\"connection-builder\"]/div/div/div[1]/div[2]/div/div/div/div[1]/div/div/div/div/div/ul/li[2]/div[2]/div/div/button/span")).click();
        // 8 | doubleClick | xpath=/html/body/div[6]/div[3]/div/div/div/div[3]/div/div/div[2]/div/div[1]/div/div[2]/div[2]/div[1]/div/p |
        {
            WebElement element = navegador.findElement(By.xpath("/html/body/div[6]/div[3]/div/div/div/div[3]/div/div/div[2]/div/div[1]/div/div[2]/div[2]/div[1]/div/p"));
            Actions builder = new Actions(navegador);
            builder.doubleClick(element).perform();
        }
        // 9 | selectFrame | index=3 |
        navegador.switchTo().frame(3);
        // 10 | selectFrame | relative=parent |
        navegador.switchTo().defaultContent();
        // 11 | verifyElementPresent | css=.jss793 > .MuiButton-label |
        {
            List<WebElement> elements = navegador.findElements(By.cssSelector(".jss793 > .MuiButton-label"));
            assert(elements.size() > 0);
        }
        // 12 | verifyElementPresent | css=.jss561 > rect |
        {
            List<WebElement> elements = navegador.findElements(By.cssSelector(".jss561 > rect"));
            assert(elements.size() > 0);
        }
        // 13 | verifyElementPresent | css=.jss595 > .MuiButton-label |
        {
            List<WebElement> elements = navegador.findElements(By.cssSelector(".jss595 > .MuiButton-label"));
            assert(elements.size() > 0);
        }
        // 14 | verifyElementPresent | css=.MuiListSubheader-root:nth-child(3) |
        {
            List<WebElement> elements = navegador.findElements(By.cssSelector(".MuiListSubheader-root:nth-child(3)"));
            assert(elements.size() > 0);
        }
        // 15 | verifyElementPresent | css=.MuiListItem-root:nth-child(4) > .MuiListItemIcon-root > .MuiSvgIcon-root |
        {
            List<WebElement> elements = navegador.findElements(By.cssSelector(".MuiListItem-root:nth-child(4) > .MuiListItemIcon-root > .MuiSvgIcon-root"));
            assert(elements.size() > 0);
        }


        // 16 | click | xpath=//*[@id="connection-builder"]/div/div/div[2]/button/span |
        navegador.findElement(By.xpath("//*[@id=\"connection-builder\"]/div/div/div[2]/button/span")).click();

        // 17 | verifyElementPresent | css=.MuiGrid-root:nth-child(2) > .MuiGrid-root:nth-child(2) > .MuiGrid-root > .MuiGrid-root rect |
        {
            List<WebElement> elements = navegador.findElements(By.cssSelector(".MuiGrid-root:nth-child(2) > .MuiGrid-root:nth-child(2) > .MuiGrid-root > .MuiGrid-root rect"));
            assert(elements.size() > 0);
        }
        // 18 | verifyElementPresent | css=.MuiGrid-root:nth-child(2) > .MuiGrid-root:nth-child(3) rect |
        {
            List<WebElement> elements = navegador.findElements(By.cssSelector(".MuiGrid-root:nth-child(2) > .MuiGrid-root:nth-child(3) rect"));
            assert(elements.size() > 0);
        }
        // 19 | verifyElementPresent | css=.MuiGrid-root:nth-child(2) > .MuiGrid-root:nth-child(4) rect |
        {
            List<WebElement> elements = navegador.findElements(By.cssSelector(".MuiGrid-root:nth-child(2) > .MuiGrid-root:nth-child(4) rect"));
            assert(elements.size() > 0);
        }
        // 20 | verifyElementPresent | css=.MuiGrid-root:nth-child(5) .jss821 |
        {
            List<WebElement> elements = navegador.findElements(By.cssSelector(".MuiGrid-root:nth-child(5) .jss821"));
            assert(elements.size() > 0);
        }





    }


    @Test
    public void googleSheetsWithGoogleSheets() {
        //Clicar em Google Sheets no send data

        //Clicar em finish and save
    }

    // @Test
    public void googleSheetsWithExcel() {
        //Clicar em Excel no send data

        //Clicar em finish and save

    }

    //@Test
    public void googleSheetsWithCsv() {
        //Clicar em CSV File no send data

        //Clicar em finish and save
    }

    //@Test
    public void googleSheetsWithMultiFiles() {
        //Clicar em Multiple spreadsheets in a folder no send data

        //Clicar em finish and save
    }



    //@After
    public void tearDown(){
        //Fechar o navegador
        //close fecha uma e o quit fecha todas
        navegador.quit();
    }
}

