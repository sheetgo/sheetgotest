package tests;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class SheetgoLoginsTest {

    private WebDriver navegador;

    @Before
    public void setUp(){
        //Abrindo o navegador
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Drivers\\chromedriver.exe");
        navegador = new ChromeDriver();
        navegador.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //Navegando para a pagina do taskit
        navegador.get("http://dev.sheetgo.com");
    }

    @Test
    public void loginGoogle(){

        //Clicar no botao para logar com o google com o xpath "//*[@id="google-login"]/span/div[2]"
        navegador.findElement(By.xpath("//*[@id=\"google-login\"]/span/div[2]")).click();

        //Abrir popup para login
        System.out.println(navegador.getWindowHandle());
        System.out.println(navegador.getWindowHandles());
        navegador.switchTo().window((String) navegador.getWindowHandles().toArray()[1]);

        //Verificar se abriu o oauth2 com o titulo Fazer login
        WebElement mensagemPop = navegador.findElement(By.xpath("//*[@id=\"headingText\"]/span"));
        String mensagem = mensagemPop.getText();
        assertEquals("Fazer login", mensagem);

        //Escrever no input com xpath "//*[@id="identifierId"]" sheetgotest@gmail.com
        navegador.findElement(By.xpath("//*[@id=\"identifierId\"]")).sendKeys("sheetgotest@gmail.com");

        //Clicar no button com xpath "//*[@id="identifierNext"]/div/button"
        navegador.findElement(By.xpath("//*[@id=\"identifierNext\"]/div/button")).click();

        //Escrever no input com name "password" Sheetgo2020
        navegador.findElement(By.name("password")).sendKeys("Sheetgo2020");

        //Clicar no button com xpath "//*[@id="passwordNext"]/div/button"
        navegador.findElement(By.xpath("//*[@id=\"passwordNext\"]/div/button")).click();


    }

    //@Test
    public void loginDropbox(){

        //Clicar no botao para logar com o Dropbox com o xpath "//*[@id="dropbox-login-button"]"
        navegador.findElement(By.xpath("//*[@id=\"dropbox-login-button\"]")).click();

        //Abrir popup para login
        System.out.println(navegador.getWindowHandle());
        System.out.println(navegador.getWindowHandles());
        navegador.switchTo().window((String) navegador.getWindowHandles().toArray()[1]);

        //Escrever no input com name "login_email" xpath "//*[@id="pyxl1026949413522686048"]" sheetgotest@gmail.com
        navegador.findElement(By.xpath("//*[@id=\"pyxl5497503626553292048\"]/div[2]")).sendKeys("sheetgotest@gmail.com");

        //Escrever no input com xpath Sheetgo2020
        navegador.findElement(By.name("login_password")).sendKeys("Sheetgo2020");

        //Clicar no button com xpath "//*[@id="regular-login-forms"]/div/form/div[3]/button"
        navegador.findElement(By.cssSelector(".login-button > .sign-in-text")).click();

    }
    @Test
    public void loginMicrosoft(){

        //Clicar no botao para logar com o Microsoft com o xpath "//*[@id="microsoft-login-button"]"
        navegador.findElement(By.xpath("//*[@id=\"microsoft-login-button\"]")).click();

        //Abrir popup para login
        System.out.println(navegador.getWindowHandle());
        System.out.println(navegador.getWindowHandles());
        navegador.switchTo().window((String) navegador.getWindowHandles().toArray()[1]);

        //Verificar se abriu o oauth2 com o titulo "Sign in"
        WebElement mensagemPop = navegador.findElement(By.xpath("//*[@id=\"loginHeader\"]/div"));
        String mensagem = mensagemPop.getText();
        assertEquals("Sign in", mensagem);

        //Escrever no input com id "i0116" sheetgotest@gmail.com
        navegador.findElement(By.id("i0116")).sendKeys("sheetgotest@gmail.com");

        //Clicando em Next com id "idSIButton9"
        navegador.findElement(By.id("idSIButton9")).click();

        {
            WebDriverWait wait = new WebDriverWait(navegador, 3);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("displayName")));
        }

        // Verificar
        //assertThat(navegador.findElement(By.id("displayName")).getText(), is("sheetgotest@gmail.com"));

        //Escrever no input com id "i0118"
        navegador.findElement(By.id("i0118")).sendKeys("Sheetgo2020");

        //Clicar no button com id "dSIButton9"
        navegador.findElement(By.id("idSIButton9")).click();

    }

    @After
    public void tearDown(){
        //Fechar o navegador
        //close fecha uma e o quit fecha todas
        navegador.quit();
    }
}
