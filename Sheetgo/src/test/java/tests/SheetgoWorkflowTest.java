package tests;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class SheetgoWorkflowTest {

    private WebDriver navegador;

    @Before
    public void setUp(){
        //Abrindo o navegador
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Drivers\\chromedriver.exe");
        navegador = new ChromeDriver();
        navegador.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        //Navegando para a pagina do taskit
        navegador.get("http://dev.sheetgo.com");

        //Clicar no botao para logar com o google com o xpath "//*[@id="google-login"]/span/div[2]"
        navegador.findElement(By.xpath("//*[@id=\"google-login\"]/span/div[2]")).click();

        //Abrir popup para login
        System.out.println(navegador.getWindowHandle());
        System.out.println(navegador.getWindowHandles());
        navegador.switchTo().window((String) navegador.getWindowHandles().toArray()[1]);

        //Verificar se abriu o oauth2 com o titulo Fazer login
        WebElement mensagemPop = navegador.findElement(By.xpath("//*[@id=\"headingText\"]/span"));
        String mensagem = mensagemPop.getText();
        assertEquals("Fazer login", mensagem);

        //Escrever no input com xpath "//*[@id="identifierId"]" sheetgotest@gmail.com
        navegador.findElement(By.xpath("//*[@id=\"identifierId\"]")).sendKeys("user-sheetgo-160@gedu-demo-sheetgo.com");

        //Clicar no button com xpath "//*[@id="identifierNext"]/div/button"
        navegador.findElement(By.xpath("//*[@id=\"identifierNext\"]/div/button")).click();

        //Escrever no input com name "password" Sheetgo2020
        navegador.findElement(By.name("password")).sendKeys("toolsmoon");

        //Clicar no button com xpath "//*[@id="passwordNext"]/div/button"
        navegador.findElement(By.xpath("//*[@id=\"passwordNext\"]/div/button")).click();

        //Retornar para a tela oficial
        navegador.switchTo().window((String) navegador.getWindowHandles().toArray()[0]);

    }



    @Test
    public void createWorkflow() {

        // Criando um novo Workflow no button "Create workflow"
        navegador.findElement(By.cssSelector("#sidebar-menu > div:nth-child(2) > button > span > span")).click();

    }

    @Test
    public void renameWorkflow() {

        //Clicar nos tres pontos
        navegador.findElement(By.xpath("//*[@id=\"sidebar-menu\"]/div[3]/div[2]/div/div/div/div[3]/div/div/div[1]/button")).click();

        //Clicar em "Edit details"
        navegador.findElement(By.id("workflow-menu-settings-mobile")).click();

        //Limpar input de name
        navegador.findElement(By.id("workflow-settings-input")).clear();

        //Escrever Teste 1
        navegador.findElement(By.id("workflow-settings-input")).sendKeys("Teste 1");

        //Escrever n/a em description
        navegador.findElement(By.id("workflow-settings-description")).sendKeys("n/a");

        //Escrever n/a em other
        navegador.findElement(By.id("workflow-settings-other")).sendKeys("n/a");

        //Clicar em "Save"
        navegador.findElement(By.xpath("//*[@id=\"workflow-settings-dialog\"]/div[3]/div/div[3]/button[2]/span")).click();
    }

   // @Test
    public void duplicateWorkflow(){
        //Clicar nos tres pontos
        navegador.findElement(By.xpath("//*[@id=\"sidebar-menu\"]/div[3]/div[2]/div/div/div/div[3]/div/div/div[1]/button")).click();

        //Clicar em "duplicate Workflow"
        navegador.findElement(By.xpath("")).click();

        //Ver se aparece a mensagem "Your workflow is being duplicated, you can keep using Sheetgo in the meantime"
        navegador.findElement(By.xpath("")).click();

        //Ver se aparece a mensagem "Your workflow has been duplicated"
        navegador.findElement(By.xpath("")).click();

    }

    @After
    public void tearDown(){
        //Fechar o navegador
        //close fecha uma e o quit fecha todas
        navegador.quit();
    }
}
